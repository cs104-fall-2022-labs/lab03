midterm_1_weight = 20
midterm_2_weight = 20
final_weight = 40
homework_1_weight = 5
homework_2_weight = 5
homework_3_weight = 5
homework_4_weight = 5


def calculate_grade(midterm_1, midterm_2, final, homework_1, homework_2, homework_3, homework_4):
    if final < 40:
        return "F"

    total_score = (midterm_1 * midterm_1_weight + midterm_2 * midterm_2_weight + final * final_weight + homework_1 * homework_1_weight + homework_2 * homework_2_weight + homework_3 * homework_3_weight + homework_4 * homework_4_weight)/100

    if total_score >= 90:
        return "A"
    elif 90 > total_score >= 85:
        return "A-"
    elif 85 > total_score >= 80:
        return "B+"
    elif 80 > total_score >= 75:
        return "B"
    elif 75 > total_score >= 70:
        return "B-"
    elif 70 > total_score >= 65:
        return "C+"
    elif 65 > total_score >= 60:
        return "C"
    elif 60 > total_score >= 55:
        return "C-"
    elif 55 > total_score >= 50:
        return "D+"
    elif 50 > total_score >= 45:
        return "D"
    else:
        return "F"


midterm_1 = float(input("Please enter midterm 1: "))
midterm_2 = float(input("Please enter midterm 2: "))
final = float(input("Please enter final: "))
hw1 = float(input("Please enter homework 1: "))
hw2 = float(input("Please enter homework 2: "))
hw3 = float(input("Please enter homework 3: "))
hw4 = float(input("Please enter homework 4: "))

my_letter_grade = calculate_grade(midterm_1, midterm_2, final, hw1, hw2, hw3, hw4)

print("My final letter grade is: ", my_letter_grade)
