Number1 = input("Please enter your first number: ")
print(type(Number1))
Number1 = float(Number1)
print(type(Number1))

Number2 = input("Please enter your second number: ")
print(type(Number2))
Number2 = float(Number2)
print(type(Number2))

operation_type = input("Please enter the operation type: ")

if operation_type == "addition":
    print("Result of addition is", Number1 + Number2)
elif operation_type == "subtraction":
    print("Result of subtraction is", Number1 - Number2)
elif operation_type == "multiplication":
    print("Result of multiplication is", Number1 * Number2)
elif operation_type == "division":
    print("Result of division is", Number1 / Number2)
else:
    print("The operation is not supported!")
