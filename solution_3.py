def season(month, day):
    if 1 <= month <= 2:
        return "winter"
    elif 4 <= month <= 5:
        return "spring"
    elif 7 <= month <= 8:
        return "summer"
    elif 10 <= month <= 11:
        return "fall"
    elif month == 12:
        if day >= 16:
            return "winter"
        else:
            return "fall"
    elif month == 3:
        if day >= 16:
            return "spring"
        else:
            return "winter"
    elif month == 6:
        if day >= 16:
            return "summer"
        else:
            return "spring"
    elif month == 9:
        if day >= 16:
            return "fall"
        else:
            return "summer"


month = 3
day = 15
print(season(month, day))  # expect winter
day = 16
print(season(month, day))  # expect spring

month = 6
day = 15
print(season(month, day))  # expect spring
day = 16
print(season(month, day))  # expect summer

month = 9
day = 15
print(season(month, day))  # expect summer
day = 16
print(season(month, day))  # expect fall

month = 12
day = 15
print(season(month, day))  # expect fall
day = 16
print(season(month, day))  # expect winter

month = 12
day = 12
print(season(month, day))  # expect fall
month = 1
day = 24
print(season(month, day))  # expect winter
month = 8
day = 29
print(season(month, day))  # expect summer
