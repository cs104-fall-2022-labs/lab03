def find_min_mid_max(my_first_number, my_second_number, my_third_number):
    if my_first_number >= my_second_number >= my_third_number:
        return my_third_number, my_second_number, my_first_number
    elif my_first_number >= my_third_number >= my_second_number:
        return my_second_number, my_third_number, my_first_number
    elif my_second_number >= my_third_number >= my_first_number:
        return my_first_number, my_third_number, my_second_number
    elif my_second_number >= my_first_number >= my_third_number:
        return my_third_number, my_first_number, my_second_number
    elif my_third_number >= my_first_number >= my_second_number:
        return my_second_number, my_first_number, my_third_number
    elif my_third_number >= my_second_number >= my_first_number:
        return my_first_number, my_second_number, my_third_number


my_first_number = input("Enter first number: ")
my_second_number = input("Enter second number: ")
my_third_number = input("Enter third number: ")

minimum, median, maximum = find_min_mid_max(my_first_number, my_second_number, my_third_number)

print("Minimum Value is ", minimum)
print("Median Value is ", median)
print("Maximum Value is ", maximum)
